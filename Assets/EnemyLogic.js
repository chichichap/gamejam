﻿#pragma strict

var leader : Transform;

var follower : Transform;

var speed : float = 1; // The speed of the follower

/*
// Transforms to act as start and end markers for the journey.
var startMarker: Transform;
var endMarker: Transform;
var walkingBack: boolean;

// Movement speed in units/sec.
var speed = 1000.0;

// Time when the movement started.
private var startTime: float;

// Total distance between the markers.
private var journeyLength: float;

var target : Transform;
var smooth = 5.0;*/


//var ZombieTransform : Transform;
var g_health = 100;

protected var animator: Animator;

function OnTriggerEnter (theCollider : Collider) {
	if ( theCollider.tag == "Player")
		Application.LoadLevel("GameOver");  
}

function Start() {
	/*walkingBack = false;
	// Keep a note of the time the movement started.
	startTime = Time.time;

	// Calculate the journey length.
	journeyLength = Vector3.Distance(Vector3(0.5, 0.5, 7), Vector3(0.5, 0.5, 17));*/
		
	animator = GetComponent(Animator);
}

function ApplyDamage ( damage : int) {
	g_health -= damage;
	
	// can't find GetHit
	//ZombieTransform.animation.CrossFade("GetHit");
	//yield WaitForSeconds(3);
	//ZombieTransform.animation.CrossFade("Recover");
	
	animator.SetTrigger("isHitTrigger");
}

function Update () {
	var distance = Vector3.Distance(leader.position, follower.position);
	
	if ( distance < 10 ) {
		follower.LookAt(leader);
		follower.LookAt( Vector3(leader.position.x, 0, leader.position.z) );
    	//follower.Translate(speed*Vector3.forward*Time.deltaTime);
    }
/*
	// Distance moved = time * speed.
	var distCovered = (Time.time - startTime) * speed;
		
	// Fraction of journey completed = current distance divided by total distance.
	var fracJourney = distCovered / journeyLength;
		
	
	
	// Set our position as a fraction of the distance between the markers.
	if ( walkingBack )
		transform.position = Vector3.Lerp(Vector3(0.5, 0.5, 17) , Vector3(0.5, 0.5, 7), fracJourney);
	else 
		transform.position = Vector3.Lerp(Vector3(0.5, 0.5, 7) , Vector3(0.5, 0.5, 17), fracJourney);
	
	
	
	if (transform.position == Vector3(0.5, 0.5, 17)) {
		walkingBack = true;
		startTime = Time.time;
	} 
	
		if (transform.position == Vector3(0.5, 0.5, 7)) {
		walkingBack = false;
		startTime = Time.time;
	} */
	
	if ( g_health <= 0 )
		Die();
}

function Die() {
	yield WaitForSeconds(1);
	Destroy( gameObject );
}